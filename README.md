# PSOC_RNG

A PSOC implementation of a (true) random number generator.

It is a Ring Oscillator coupled to a Linear Hybrid Cellular Automata.

```mermaid
graph LR
    linkStyle default interpolate basis
    XOR1-->FFD1;
    CLK-->FFD1;
    FFD1-->XOR2;
    XOR2-->FFD2;
    CLK-->FFD2;
    FFD2-->OUT;
```



Based on https://pdfs.semanticscholar.org/83ac/9e9c1bb3dad5180654984604c8d5d8137412.pdf

**Attention: BETA VERSION**
Tested on PSOC Creator 4.2